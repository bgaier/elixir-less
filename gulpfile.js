const elixir = require('laravel-elixir');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 */

const path = './themes/mytheme/'

elixir(mix => {
    mix.less(path + 'less/theme.less', path + 'bootstrap.css')
        .browserSync({
            watch: true,
            proxy: 'localhost:80/airsoft-sports.com-jtl-shop/',
            files: ['**/*.tpl', '**/*.css']
        });
});
